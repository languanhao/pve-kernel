RELEASE=4.1

# also update proxmox-ve/changelog if you change KERNEL_VER or KREL
KERNEL_VER=4.2.6
PKGREL=28
# also include firmware of previous version into
# the fw package:  fwlist-2.6.32-PREV-pve
KREL=1

KERNEL_SRC=ubuntu-wily
KERNELSRCTAR=${KERNEL_SRC}.tgz

EXTRAVERSION=-${KREL}-pve
KVNAME=${KERNEL_VER}${EXTRAVERSION}
PACKAGE=pve-kernel-${KVNAME}
HDRPACKAGE=pve-headers-${KVNAME}

ARCH=amd64
GITVERSION:=$(shell cat .git/refs/heads/master)

TOP=$(shell pwd)

KERNEL_CFG_ORG=config-${KERNEL_VER}.org

FW_VER=1.1
FW_REL=7
FW_DEB=pve-firmware_${FW_VER}-${FW_REL}_all.deb

# does not compile with kernel 3.19.8
E1000EDIR=e1000e-3.2.7.1
E1000ESRC=${E1000EDIR}.tar.gz

#IGBDIR=igb-5.3.3.2
#IGBSRC=${IGBDIR}.tar.gz

IXGBEDIR=ixgbe-4.1.5
IXGBESRC=${IXGBEDIR}.tar.gz

# does not compile with kernel 3.19.8
#I40EDIR=i40e-1.2.38
#I40ESRC=${I40EDIR}.tar.gz

# looks up to date with kernel 3.19.8
#BNX2DIR=netxtreme2-7.11.05
#BNX2SRC=${BNX2DIR}.tar.gz

# does not compile with kernel 3.19.8
#AACRAIDVER=1.2.1-40700
#AACRAIDDIR=aacraid-${AACRAIDVER}.src
#AACRAIDSRC=aacraid-linux-src-${AACRAIDVER}.tgz

# does not compile with kernel 3.19.8
HPSAVER=3.4.8
HPSADIR=hpsa-${HPSAVER}
HPSASRC=${HPSADIR}-140.tar.bz2

# driver does not compile
#MEGARAID_DIR=megaraid_sas-06.703.11.00
#MEGARAID_SRC=${MEGARAID_DIR}-src.tar.gz

#ARECADIR=arcmsr-1.30.0X.19-140509
#ARECASRC=${ARECADIR}.zip

# this one does not compile with newer 3.10 kernels!
#RR272XSRC=RR272x_1x-Linux-Src-v1.5-130325-0732.tar.gz
#RR272XDIR=rr272x_1x-linux-src-v1.5

SPLDIR=pkg-spl
SPLSRC=pkg-spl.tar.gz
ZFSDIR=pkg-zfs
ZFSSRC=pkg-zfs.tar.gz
ZFS_MODULES=zfs.ko zavl.ko znvpair.ko zunicode.ko zcommon.ko zpios.ko
SPL_MODULES=spl.ko splat.ko

# DRBD9
DRBDVER=9.0.0
DRBDDIR=drbd-${DRBDVER}
DRBDSRC=${DRBDDIR}.tar.gz
DRBD_MODULES=drbd.ko drbd_transport_tcp.ko

DST_DEB=${PACKAGE}_${KERNEL_VER}-${PKGREL}_${ARCH}.deb
HDR_DEB=${HDRPACKAGE}_${KERNEL_VER}-${PKGREL}_${ARCH}.deb
PVEPKG=proxmox-ve
PVE_DEB=${PVEPKG}_${RELEASE}-${PKGREL}_all.deb

LINUX_TOOLS_PKG=linux-tools-4.2
LINUX_TOOLS_DEB=${LINUX_TOOLS_PKG}_${KERNEL_VER}-${PKGREL}_amd64.deb

all: check_gcc ${DST_DEB} ${FW_DEB} ${HDR_DEB} ${PVE_DEB} ${LINUX_TOOLS_DEB}

${PVE_DEB} pve: proxmox-ve/control proxmox-ve/postinst
	rm -rf proxmox-ve/data
	mkdir -p proxmox-ve/data/DEBIAN
	mkdir -p proxmox-ve/data/usr/share/doc/${PVEPKG}/
	install -m 0644 proxmox-ve/proxmox-release\@proxmox.com.pubkey proxmox-ve/data/usr/share/doc/${PVEPKG}
	sed -e 's/@KVNAME@/${KVNAME}/' -e 's/@KERNEL_VER@/${KERNEL_VER}/' -e 's/@RELEASE@/${RELEASE}/' -e 's/@PKGREL@/${PKGREL}/' <proxmox-ve/control >proxmox-ve/data/DEBIAN/control
	install -m 0755 proxmox-ve/postinst proxmox-ve/data/DEBIAN/postinst
	echo "git clone git://git.proxmox.com/git/pve-kernel-4.0.git\\ngit checkout ${GITVERSION}" > proxmox-ve/data/usr/share/doc/${PVEPKG}/SOURCE
	install -m 0644 proxmox-ve/copyright proxmox-ve/data/usr/share/doc/${PVEPKG}
	install -m 0644 proxmox-ve/changelog.Debian proxmox-ve/data/usr/share/doc/${PVEPKG}
	gzip --best proxmox-ve/data/usr/share/doc/${PVEPKG}/changelog.Debian
	dpkg-deb --build proxmox-ve/data ${PVE_DEB}

# see https://wiki.ubuntu.com/Kernel/Dev/KernelGitGuide
# changelog: https://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/wily/log/
.PHONY: download
download:
	rm -rf ${KERNEL_SRC} ${KERNELSRCTAR}
	#git clone git://kernel.ubuntu.com/ubuntu/ubuntu-vivid.git
	git clone git://git.launchpad.net/~ubuntu-kernel/ubuntu/+source/linux/+git/wily ${KERNEL_SRC}
	tar czf ${KERNELSRCTAR} --exclude .git ${KERNEL_SRC} 

check_gcc: 
ifeq    ($(CC), cc)
	gcc --version|grep "4\.9" || false
else
	$(CC) --version|grep "4\.9" || false
endif

${DST_DEB}: data control.in prerm.in postinst.in postrm.in copyright changelog.Debian
	mkdir -p data/DEBIAN
	sed -e 's/@KERNEL_VER@/${KERNEL_VER}/' -e 's/@KVNAME@/${KVNAME}/' -e 's/@PKGREL@/${PKGREL}/' <control.in >data/DEBIAN/control
	sed -e 's/@@KVNAME@@/${KVNAME}/g'  <prerm.in >data/DEBIAN/prerm
	chmod 0755 data/DEBIAN/prerm
	sed -e 's/@@KVNAME@@/${KVNAME}/g'  <postinst.in >data/DEBIAN/postinst
	chmod 0755 data/DEBIAN/postinst
	sed -e 's/@@KVNAME@@/${KVNAME}/g'  <postrm.in >data/DEBIAN/postrm
	chmod 0755 data/DEBIAN/postrm
	install -D -m 644 copyright data/usr/share/doc/${PACKAGE}/copyright
	install -D -m 644 changelog.Debian data/usr/share/doc/${PACKAGE}/changelog.Debian
	echo "git clone git://git.proxmox.com/git/pve-kernel.git\\ngit checkout ${GITVERSION}" > data/usr/share/doc/${PACKAGE}/SOURCE
	gzip -f --best data/usr/share/doc/${PACKAGE}/changelog.Debian
	rm -f data/lib/modules/${KVNAME}/source
	rm -f data/lib/modules/${KVNAME}/build
	dpkg-deb --build data ${DST_DEB}
	lintian ${DST_DEB}

LINUX_TOOLS_DH_LIST=strip installchangelogs installdocs compress shlibdeps gencontrol md5sums builddeb

${LINUX_TOOLS_DEB}: .compile_mark control.tools changelog.Debian copyright
	rm -rf linux-tools ${LINUX_TOOLS_DEB}
	mkdir -p linux-tools/debian
	cp control.tools linux-tools/debian/control
	echo 9 > linux-tools/debian/compat
	cp changelog.Debian linux-tools/debian/changelog
	cp copyright linux-tools/debian
	mkdir -p linux-tools/debian/linux-tools-4.2/usr/bin
	install -m 0755 ${KERNEL_SRC}/tools/perf/perf linux-tools/debian/linux-tools-4.2/usr/bin/perf_4.2
	cd linux-tools; for i in ${LINUX_TOOLS_DH_LIST}; do dh_$$i; done
	lintian ${LINUX_TOOLS_DEB}

fwlist-${KVNAME}: data
	./find-firmware.pl data/lib/modules/${KVNAME} >fwlist.tmp
	mv fwlist.tmp $@

data: .compile_mark ixgbe.ko e1000e.ko ${SPL_MODULES} ${ZFS_MODULES} ${DRBD_MODULES}
	rm -rf data tmp; mkdir -p tmp/lib/modules/${KVNAME}
	mkdir tmp/boot
	install -m 644 ${KERNEL_SRC}/.config tmp/boot/config-${KVNAME}
	install -m 644 ${KERNEL_SRC}/System.map tmp/boot/System.map-${KVNAME}
	install -m 644 ${KERNEL_SRC}/arch/x86_64/boot/bzImage tmp/boot/vmlinuz-${KVNAME}
	cd ${KERNEL_SRC}; make INSTALL_MOD_PATH=../tmp/ modules_install
	## install latest ibg driver
	#install -m 644 igb.ko tmp/lib/modules/${KVNAME}/kernel/drivers/net/ethernet/intel/igb/
	# install latest ixgbe driver
	install -m 644 ixgbe.ko tmp/lib/modules/${KVNAME}/kernel/drivers/net/ethernet/intel/ixgbe/
	# install latest e1000e driver
	install -m 644 e1000e.ko tmp/lib/modules/${KVNAME}/kernel/drivers/net/ethernet/intel/e1000e/
	## install hpsa driver
	#install -m 644 hpsa.ko tmp/lib/modules/${KVNAME}/kernel/drivers/scsi/
	# install zfs drivers
	install -d -m 0755 tmp/lib/modules/${KVNAME}/zfs
	install -m 644 ${SPL_MODULES} ${ZFS_MODULES} tmp/lib/modules/${KVNAME}/zfs
	# install drbd9
	install -m 644 ${DRBD_MODULES} tmp/lib/modules/${KVNAME}/kernel/drivers/block/drbd
	# remove firmware
	rm -rf tmp/lib/firmware
	# strip debug info
	find tmp/lib/modules -name \*.ko -print | while read f ; do strip --strip-debug "$$f"; done
	# finalize
	depmod -b tmp/ ${KVNAME}
	# Autogenerate blacklist for watchdog devices (see README)
	install -m 0755 -d tmp/lib/modprobe.d
	ls tmp/lib/modules/${KVNAME}/kernel/drivers/watchdog/ > watchdog-blacklist.tmp
	echo ipmi_watchdog.ko >> watchdog-blacklist.tmp
	cat watchdog-blacklist.tmp|sed -e 's/^/blacklist /' -e 's/.ko$$//'|sort -u > tmp/lib/modprobe.d/blacklist_${PACKAGE}.conf
	mv tmp data

PVE_CONFIG_OPTS= \
-d CONFIG_SND_PCM_OSS \
-d CONFIG_TRANSPARENT_HUGEPAGE_MADVISE \
-d CONFIG_TRANSPARENT_HUGEPAGE_ALWAYS \
-e CONFIG_TRANSPARENT_HUGEPAGE_NEVER \
-m CONFIG_CEPH_FS \
-m CONFIG_BLK_DEV_NBD \
-m CONFIG_BLK_DEV_RBD \
-m CONFIG_BCACHE \
-m CONFIG_JFS_FS \
-m CONFIG_HFS_FS \
-m CONFIG_HFSPLUS_FS \
-e CONFIG_BRIDGE \
-e CONFIG_BRIDGE_NETFILTER \
-e CONFIG_BLK_DEV_SD \
-e CONFIG_BLK_DEV_SR \
-e CONFIG_BLK_DEV_DM \
-d CONFIG_INPUT_EVBUG \
-d CONFIG_CPU_FREQ_DEFAULT_GOV_ONDEMAND \
-e CONFIG_CPU_FREQ_DEFAULT_GOV_PERFORMANCE \
-d CONFIG_MODULE_SIG \
-d CONFIG_MEMCG_DISABLED \
-e CONFIG_MEMCG_SWAP_ENABLED \
-e CONFIG_MEMCG_KMEM \
-d CONFIG_DEFAULT_CFQ \
--set-str CONFIG_DEFAULT_IOSCHED deadline \
-d CONFIG_DEFAULT_SECURITY_DAC \
-e CONFIG_DEFAULT_SECURITY_APPARMOR \
--set-str CONFIG_DEFAULT_SECURITY apparmor

.compile_mark: ${KERNEL_SRC}/README ${KERNEL_CFG_ORG}
	cp ${KERNEL_CFG_ORG} ${KERNEL_SRC}/.config
	cd ${KERNEL_SRC}; ./scripts/config ${PVE_CONFIG_OPTS}
	cd ${KERNEL_SRC}; make oldconfig
	cd ${KERNEL_SRC}; make -j 8
	make -C ${KERNEL_SRC}/tools/perf prefix=/usr HAVE_CPLUS_DEMANGLE=1 NO_LIBPYTHON=1 NO_LIBPERL=1 PYTHON=python2.7
	make -C ${KERNEL_SRC}/tools/perf man
	touch $@

${KERNEL_SRC}/README ${KERNEL_CFG_ORG}: ${KERNELSRCTAR} 
	rm -rf ${KERNEL_SRC}
	tar xf ${KERNELSRCTAR}
	cat ${KERNEL_SRC}/debian.master/config/config.common.ubuntu ${KERNEL_SRC}/debian.master/config/amd64/config.common.amd64 ${KERNEL_SRC}/debian.master/config/amd64/config.flavour.generic > ${KERNEL_CFG_ORG}
	cd ${KERNEL_SRC}; patch -p1 <../add-thp-never-option.patch
	cd ${KERNEL_SRC}; patch -p1 <../bridge-patch.diff
	#cd ${KERNEL_SRC}; patch -p1 <../bridge-forward-ipv6-neighbor-solicitation.patch
	#cd ${KERNEL_SRC}; patch -p1 <../add-empty-ndo_poll_controller-to-veth.patch
	#cd ${KERNEL_SRC}; patch -p1 <../override_for_missing_acs_capabilities.patch
	#cd ${KERNEL_SRC}; patch -p1 <../vhost-net-extend-device-allocation-to-vmalloc.patch
	cd ${KERNEL_SRC}; patch -p1 <../kvmstealtime.patch
	cd ${KERNEL_SRC}; patch -p1 <../kvm-x86-obey-KVM_X86_QUIRK_CD_NW_CLEARED-in-kvm_set_cr0.patch
	cd ${KERNEL_SRC}; patch -p1 <../KVM-svm-unconditionally-intercept-DB.patch
	cd ${KERNEL_SRC}; patch -p1 <../apparmor-socket-mediation.patch
	# backport aacraid update from kernel 4.4rc5
	cd ${KERNEL_SRC}; patch -p1 <../0001-aacraid-fix-for-LD.patch
	cd ${KERNEL_SRC}; patch -p1 <../0002-aacraid-add-power-management.patch
	cd ${KERNEL_SRC}; patch -p1 <../0003-aacraid-change-interrupt-mode.patch
	cd ${KERNEL_SRC}; patch -p1 <../0004-aacraid-enable-64bit-write.patch
	cd ${KERNEL_SRC}; patch -p1 <../0005-aacraid-tune-response-path.patch
	cd ${KERNEL_SRC}; patch -p1 <../0006-aacraid-reset-irq-affinity.patch
	cd ${KERNEL_SRC}; patch -p1 <../0007-aacraid-ioctl-fix.patch
	cd ${KERNEL_SRC}; patch -p1 <../0008-aacraid-use-pci-enable-msix-range.patch
	cd ${KERNEL_SRC}; patch -p1 <../0009-aacraid-update-driver-version.patch
	sed -i ${KERNEL_SRC}/Makefile -e 's/^EXTRAVERSION.*$$/EXTRAVERSION=${EXTRAVERSION}/'
	touch $@

aacraid.ko: .compile_mark ${AACRAIDSRC}
	rm -rf ${AACRAIDDIR}
	mkdir ${AACRAIDDIR}
	cd ${AACRAIDDIR};tar xzf ../${AACRAIDSRC}
	cd ${AACRAIDDIR};rpm2cpio aacraid-${AACRAIDVER}.src.rpm|cpio -i
	cd ${AACRAIDDIR};tar xf aacraid_source.tgz
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	make -C ${TOP}/${KERNEL_SRC} M=${TOP}/${AACRAIDDIR} modules
	cp ${AACRAIDDIR}/aacraid.ko .

hpsa.ko hpsa: .compile_mark ${HPSASRC}
	rm -rf ${HPSADIR}
	tar xf ${HPSASRC}
#	sed -i ${HPSADIR}/drivers/scsi/hpsa_kernel_compat.h -e 's/^\/\* #define RHEL7.*/#define RHEL7/'
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	make -C ${TOP}/${KERNEL_SRC} M=${TOP}/${HPSADIR}/drivers/scsi modules
	cp ${HPSADIR}/drivers/scsi/hpsa.ko hpsa.ko

e1000e.ko e1000e: .compile_mark ${E1000ESRC}
	rm -rf ${E1000EDIR}
	tar xf ${E1000ESRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${E1000EDIR}/src; make BUILD_KERNEL=${KVNAME}
	cp ${E1000EDIR}/src/e1000e.ko e1000e.ko

igb.ko igb: .compile_mark ${IGBSRC}
	rm -rf ${IGBDIR}
	tar xf ${IGBSRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${IGBDIR}/src; make BUILD_KERNEL=${KVNAME}
	cp ${IGBDIR}/src/igb.ko igb.ko

ixgbe.ko ixgbe: .compile_mark ${IXGBESRC}
	rm -rf ${IXGBEDIR}
	tar xf ${IXGBESRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${IXGBEDIR}/src; make CFLAGS_EXTRA="-DIXGBE_NO_LRO" BUILD_KERNEL=${KVNAME}
	cp ${IXGBEDIR}/src/ixgbe.ko ixgbe.ko

i40e.ko i40e: .compile_mark ${I40ESRC}
	rm -rf ${I40EDIR}
	tar xf ${I40ESRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${I40EDIR}/src; make BUILD_KERNEL=${KVNAME}
	cp ${I40EDIR}/src/i40e.ko i40e.ko

bnx2.ko cnic.ko bnx2x.ko: ${BNX2SRC}
	rm -rf ${BNX2DIR}
	tar xf ${BNX2SRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${BNX2DIR}; make -C bnx2/src KVER=${KVNAME}
	cd ${BNX2DIR}; make -C bnx2x/src KVER=${KVNAME}
	cp `find ${BNX2DIR} -name bnx2.ko -o -name cnic.ko -o -name bnx2x.ko` .

arcmsr.ko: .compile_mark ${ARECASRC}
	rm -rf ${ARECADIR}
	mkdir ${ARECADIR}; cd ${ARECADIR}; unzip ../${ARECASRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${ARECADIR}; make -C ${TOP}/${KERNEL_SRC} SUBDIRS=${TOP}/${ARECADIR} modules
	cp ${ARECADIR}/arcmsr.ko arcmsr.ko

${SPL_MODULES}: .compile_mark ${SPLSRC}
	rm -rf ${SPLDIR}
	tar xf ${SPLSRC}
	cd ${SPLDIR}; ./autogen.sh
	cd ${SPLDIR}; ./configure --with-config=kernel --with-linux=${TOP}/${KERNEL_SRC} --with-linux-obj=${TOP}/${KERNEL_SRC}
	cd ${SPLDIR}; make
	cp ${SPLDIR}/module/spl/spl.ko spl.ko
	cp ${SPLDIR}/module/splat/splat.ko splat.ko

${ZFS_MODULES}: .compile_mark ${ZFSSRC}
	rm -rf ${ZFSDIR}
	tar xf ${ZFSSRC}
	cd ${ZFSDIR}; ./autogen.sh
	cd ${ZFSDIR}; ./configure --with-spl=${TOP}/${SPLDIR} --with-spl-obj=${TOP}/${SPLDIR} --with-config=kernel --with-linux=${TOP}/${KERNEL_SRC} --with-linux-obj=${TOP}/${KERNEL_SRC}
	cd ${ZFSDIR}; make
	cp ${ZFSDIR}/module/zfs/zfs.ko zfs.ko
	cp ${ZFSDIR}/module/avl/zavl.ko zavl.ko
	cp ${ZFSDIR}/module/nvpair/znvpair.ko znvpair.ko
	cp ${ZFSDIR}/module/unicode/zunicode.ko zunicode.ko
	cp ${ZFSDIR}/module/zcommon/zcommon.ko zcommon.ko
	cp ${ZFSDIR}/module/zpios/zpios.ko zpios.ko

.PHONY: update-drbd
update-drbd:
	rm -rf ${DRBDDIR} ${DRBDSRC} drbd-9.0
	git clone --recursive git://git.drbd.org/drbd-9.0
	cd drbd-9.0; make tarball
	mv drbd-9.0/${DRBDSRC} ${DRBDSRC} 

.PHONY: drbd
drbd ${DRBD_MODULES}: .compile_mark ${DRBDSRC}
	rm -rf ${DRBDDIR}
	tar xzf ${DRBDSRC}
	mkdir -p /lib/modules/${KVNAME}
	ln -sf ${TOP}/${KERNEL_SRC} /lib/modules/${KVNAME}/build
	cd ${DRBDDIR}; make KVER=${KVNAME}
	mv ${DRBDDIR}/drbd/drbd.ko drbd.ko
	mv ${DRBDDIR}/drbd/drbd_transport_tcp.ko drbd_transport_tcp.ko

#iscsi_trgt.ko: .compile_mark ${ISCSITARGETSRC}
#	rm -rf ${ISCSITARGETDIR}
#	tar xf ${ISCSITARGETSRC}
#	cd ${ISCSITARGETDIR}; make KSRC=${TOP}/${KERNEL_SRC}
#	cp ${ISCSITARGETDIR}/kernel/iscsi_trgt.ko iscsi_trgt.ko

headers_tmp := $(CURDIR)/tmp-headers
headers_dir := $(headers_tmp)/usr/src/linux-headers-${KVNAME}

${HDR_DEB} hdr: .compile_mark headers-control.in headers-postinst.in
	rm -rf $(headers_tmp)
	install -d $(headers_tmp)/DEBIAN $(headers_dir)/include/
	sed -e 's/@KERNEL_VER@/${KERNEL_VER}/' -e 's/@KVNAME@/${KVNAME}/' -e 's/@PKGREL@/${PKGREL}/' <headers-control.in >$(headers_tmp)/DEBIAN/control
	sed -e 's/@@KVNAME@@/${KVNAME}/g'  <headers-postinst.in >$(headers_tmp)/DEBIAN/postinst
	chmod 0755 $(headers_tmp)/DEBIAN/postinst
	install -D -m 644 copyright $(headers_tmp)/usr/share/doc/${HDRPACKAGE}/copyright
	install -D -m 644 changelog.Debian $(headers_tmp)/usr/share/doc/${HDRPACKAGE}/changelog.Debian
	echo "git clone git://git.proxmox.com/git/pve-kernel-3.10.0.git\\ngit checkout ${GITVERSION}" > $(headers_tmp)/usr/share/doc/${HDRPACKAGE}/SOURCE
	gzip -f --best $(headers_tmp)/usr/share/doc/${HDRPACKAGE}/changelog.Debian
	install -m 0644 ${KERNEL_SRC}/.config $(headers_dir)
	install -m 0644 ${KERNEL_SRC}/Module.symvers $(headers_dir)
	cd ${KERNEL_SRC}; find . -path './debian/*' -prune -o -path './include/*' -prune -o -path './Documentation' -prune \
	  -o -path './scripts' -prune -o -type f \
	  \( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
	     -name '*.sh' -o -name '*.pl' \) \
	  -print | cpio -pd --preserve-modification-time $(headers_dir)
	cd ${KERNEL_SRC}; cp -a include scripts $(headers_dir)
	cd ${KERNEL_SRC}; (find arch/x86 -name include -type d -print | \
		xargs -n1 -i: find : -type f) | \
		cpio -pd --preserve-modification-time $(headers_dir)
	dpkg-deb --build $(headers_tmp) ${HDR_DEB}
	#lintian ${HDR_DEB}

dvb-firmware.git/README:
	git clone https://github.com/OpenELEC/dvb-firmware.git dvb-firmware.git

linux-firmware.git/WHENCE:
	git clone git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git linux-firmware.git

${FW_DEB} fw: control.firmware linux-firmware.git/WHENCE dvb-firmware.git/README changelog.firmware fwlist-2.6.18-2-pve fwlist-2.6.24-12-pve fwlist-2.6.32-3-pve fwlist-2.6.32-4-pve fwlist-2.6.32-6-pve fwlist-2.6.32-13-pve fwlist-2.6.32-14-pve fwlist-2.6.32-20-pve fwlist-2.6.32-21-pve fwlist-3.10.0-3-pve fwlist-3.10.0-7-pve fwlist-3.10.0-8-pve fwlist-3.19.8-1-pve fwlist-${KVNAME}
	rm -rf fwdata
	mkdir -p fwdata/lib/firmware
	./assemble-firmware.pl fwlist-${KVNAME} fwdata/lib/firmware
	# include any files from older/newer kernels here
	./assemble-firmware.pl fwlist-2.6.18-2-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.24-12-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-3-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-4-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-6-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-13-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-14-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-20-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-2.6.32-21-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-3.10.0-3-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-3.10.0-7-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-3.10.0-8-pve fwdata/lib/firmware
	./assemble-firmware.pl fwlist-3.19.8-1-pve fwdata/lib/firmware
	install -d fwdata/usr/share/doc/pve-firmware
	cp linux-firmware.git/WHENCE fwdata/usr/share/doc/pve-firmware/README
	install -d fwdata/usr/share/doc/pve-firmware/licenses
	cp linux-firmware.git/LICEN[CS]E* fwdata/usr/share/doc/pve-firmware/licenses
	install -D -m 0644 changelog.firmware fwdata/usr/share/doc/pve-firmware/changelog.Debian
	gzip -9 fwdata/usr/share/doc/pve-firmware/changelog.Debian
	echo "git clone git://git.proxmox.com/git/pve-kernel-2.6.32.git\\ngit checkout ${GITVERSION}" >fwdata/usr/share/doc/pve-firmware/SOURCE
	install -d fwdata/DEBIAN
	sed -e 's/@VERSION@/${FW_VER}-${FW_REL}/' <control.firmware >fwdata/DEBIAN/control
	dpkg-deb --build fwdata ${FW_DEB}

.PHONY: upload
upload: ${DST_DEB} ${HDR_DEB} ${FW_DEB} ${PVE_DEB} ${LINUX_TOOLS_DEB}
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o rw 
	mkdir -p /pve/${RELEASE}/extra
	mkdir -p /pve/${RELEASE}/install
	rm -rf /pve/${RELEASE}/extra/${PACKAGE}_*.deb
	rm -rf /pve/${RELEASE}/extra/${HDRPACKAGE}_*.deb
	rm -rf /pve/${RELEASE}/extra/${PVEPKG}_*.deb
	rm -rf /pve/${RELEASE}/extra/linux-tools-*.deb
	rm -rf /pve/${RELEASE}/extra/pve-firmware*.deb
	rm -rf /pve/${RELEASE}/extra/Packages*
	cp ${DST_DEB} ${FW_DEB} ${HDR_DEB} ${PVE_DEB} ${LINUX_TOOLS_DEB} /pve/${RELEASE}/extra
	cd /pve/${RELEASE}/extra; dpkg-scanpackages . /dev/null | gzip -9c > Packages.gz
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o ro

.PHONY: distclean
distclean: clean
	rm -rf linux-firmware.git dvb-firmware.git ${KERNEL_SRC}.org 

.PHONY: clean
clean:
	rm -rf *~ .compile_mark watchdog-blacklist.tmp ${KERNEL_CFG_ORG} ${KERNEL_SRC} ${KERNEL_SRC}.tmp ${KERNEL_CFG_ORG} ${KERNEL_SRC}.org orig tmp data proxmox-ve/data *.deb ${headers_tmp} fwdata fwlist.tmp *.ko fwlist-${KVNAME} ${ZFSDIR} ${SPLDIR} ${SPL_MODULES} ${ZFS_MODULES} hpsa.ko ${HPSADIR} ${DRBDDIR} drbd-9.0 ${IGBDIR} igb.ko ${IXGBEDIR} ixgbe.ko ${E1000EDIR} e1000e.ko linux-tools ${LINUX_TOOLS_DEB}





